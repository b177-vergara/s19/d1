// console.log("Hello World");

// Exponent Operator
	// An exponent operator is added to simplify the calculation for the exponent of  a given number.
	const firstNum = Math.pow(8,2);
	console.log(firstNum);

	// ES6 update
	const secondNum = 8 ** 2;
	console.log(secondNum);

// Template Literals
	// Allows developer to write strings withour using the concat

	let name = "John";
	// Hello John! Welcome to programming!

	// let message = "Hello " + name + "! Welcome to programming!";

	// console.log("Message without template literals:\n" + message);

	// Uses backticks (``)
	// variables are placed inside a placeholder ${} 
	let message = `Hello ${name}! Welcome to programming!`;

	console.log(`Message with template literals: ${message}`);

	// Multi-line using template literals
	const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${secondNum}
`;
	console.log(anotherMessage);

	const interestRate = .1;
	const principal = 1000;

	console.log(`The interest on your savings account is: ${principal * interestRate}`);

// Array Destructuring
	// Allows us to unpack elements in arrays into distinct variables
	// Allows developer to name array elements with variables instead of using index number.

	const fullName = ["Juan", "Tolits", "Dela Cruz"];

	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);

	// Hello Juan Tolits Dela Cruz! It's good to see you again.
	console.log("Hello " + fullName[0] + " " + fullName[1] + " " + fullName[2] + "! It's good to see you again.");

	// Using Array Destructuring
	const [firstName, middleName, lastName] = fullName;

	console.log(firstName);
	console.log(middleName);
	console.log(lastName);

	// Using template literals and array destructuring
	console.log(`Hello ${firstName} ${middleName} ${lastName}! It's good to see you again.`);

// Object Destructuring
	
	const  person ={
		givenName: "Jane",
		maidenName: "Miles",
		familyName: "Doe"
	}

	console.log(person.givenName);
	console.log(person.maidenName);
	console.log(person.familyName);

	// Hello Jane Miles Doe! It's good to see you again.
	console.log(`Hello ${person.givenName} ${person.middleName} ${person.familyName}! It's good to see you again.`);

	// Object Destructuring
	// this should be exact property name of the object.
	const {givenName, maidenName, familyName} = person;

	console.log(givenName);
	console.log(maidenName);
	console.log(familyName);

	console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

	function getFullName({givenName, maidenName, familyName}){
		console.log(`This is printed inside a function`);
		console.log(`${givenName} ${maidenName} ${familyName}`);
	}

	getFullName(person);